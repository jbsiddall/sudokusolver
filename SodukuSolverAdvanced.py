
from concurrent.futures.thread import *
from concurrent.futures.process import *
from threading import Thread, Lock
import time
from math import ceil

class State(object):

	BLANK = 0
	ROW_COL_RANGE = range(1, 10);
	VALUE_RANGE = range(1, 10);

	INDEXES = [(row, col) for row in range(1, 10) for col in range(1, 10)]

	def __init__(self, state=None):
		self.state = [[self.BLANK for colIndex in self.ROW_COL_RANGE] for rowIndex in self.ROW_COL_RANGE]
		
		if state != None:
			for row in self.ROW_COL_RANGE:
				for col in self.ROW_COL_RANGE:
					self.set(row, col, state[row-1][col-1])
			

	def copy(self):
		s = State()

		for row in self.ROW_COL_RANGE:
			for col in self.ROW_COL_RANGE:
				s.set(row, col, self.get(row, col))

		return s;

	def next(self, row, col, value):
		s = self.copy()
		s.set(row, col, value)
		return s;

	def set(self, row, col, value):
		if row in self.ROW_COL_RANGE and col in self.ROW_COL_RANGE and (value in self.VALUE_RANGE or value == self.BLANK):
			oldValue = self.state[row-1][col-1]
			self.state[row-1][col-1] = value

			if not self.valid():
				self.state[row-1][col-1] = oldValue
				raise Exception("invalid state")
			else:
				return True

		else:
			raise Exception("invalid row({}), col({}) or value({})".format(row, col, value))

	def get(self, row, col):
		if row in self.ROW_COL_RANGE and col in self.ROW_COL_RANGE:
			return self.state[row-1][col-1]
		else:
			raise Exception("invalid row or col")

	
	def validGroup(self, group):
		available = [nums for nums in self.VALUE_RANGE]
		valid = True
		for item in group:
			if item in available:
				available.remove(item)
			elif item == self.BLANK:
				pass
			else:
				valid = False
		return valid


	def valid(self):
		rows = [[self.get(row, col) for col in self.ROW_COL_RANGE] for row in self.ROW_COL_RANGE]
		cols = [[self.get(row, col) for row in self.ROW_COL_RANGE] for col in self.ROW_COL_RANGE]
		groups = [ [self.get(rowIndex+rowOffset, colIndex+colOffset) for colOffset in range(3) for rowOffset in range(3)] for colIndex in range(1, 10, 3) for rowIndex in range(1, 10, 3)]

		lines = []
		lines.extend(rows)
		lines.extend(cols)
		lines.extend(groups)

		valid = True
		for line in lines: valid = valid and self.validGroup(line)

		return valid

	def __repr__(self):
		BLANK_SQUARE = " "
		toString = lambda cell: BLANK_SQUARE if cell == self.BLANK else repr(cell)
		lines = [[toString(cell) for cell in row] for row in self.state]
		
		dotLine = "-"*35
		seperator = "\n" + dotLine + "\n"
		return dotLine + "\n" + seperator.join(["|" + " | ".join(line) + "|" for line in lines]) + seperator


class Sudoku(object):

	def possibleValues(self, state, rowIndex, colIndex):

		if state.get(rowIndex, colIndex) != state.BLANK:
			raise Exception("can't get available values for square with value")

		totalAvailable = [x for x in state.VALUE_RANGE]
		row = [state.get(rowIndex, c) for c in state.ROW_COL_RANGE]
		col = [state.get(r, colIndex) for r in state.ROW_COL_RANGE]
		
		groupRow = (ceil(rowIndex / 3)-1)*3 + 1
		groupCol = (ceil(colIndex / 3)-1)*3 + 1

		group = [state.get(r, c) for r in range(groupRow, groupRow+3) for c in range(groupCol, groupCol+3)]

		for list in [row, col, group]:
			for val in list: totalAvailable.remove(val) if(val in totalAvailable) else None

		return totalAvailable

	def possibleChanges(self, state): return [(row, col, value) for (row, col) in self.empties(state) for value in self.possibleValues(state, row, col)]

	def empties(self, state): return [(row, col) for row in state.ROW_COL_RANGE for col in state.ROW_COL_RANGE if state.get(row, col) == state.BLANK]

	def assignable(self, state): return [x for x in self.empties(state) if len(self.possibleValues(state, x[0], x[1])) == 1]

	def isSolvable(self, state): return len([True for (row, col) in self.empties(state) if len(self.possibleValues(state, row, col)) == 0]) == 0

	def isSolved(self, state): return len(self.empties(state)) == 0


	""" 
		set of states
		queue of work run by main thread
		pick work with smallest number of empties 
		pick square with smalest number of available values
	"""
	def solve(self, state):

		states = {state: 1}
		solution = None

		while states and not solution:
			print(len(states))
			state = self.getNextState(states)
			states.pop(state)
			state = self.updateState(state)
			
			if(self.isSolved(state)):
				solution = state

			for (state, utility) in self.generateChildren(state).items(): states[state] = utility

		return solution


	"""
	with ThreadPoolExecutor(max_workers=1) as executor:
    future = executor.submit(pow, 323, 1235)
    print(future.result())
	"""

	def generateChildren(self, state):

		locationUtility = dict( ((row, col),1 - len(self.possibleValues(state, row, col))/9 ) for (row, col) in self.empties(state))

		children  = dict( (state.next(r, c, v), locationUtility[(r, c)]) for (r, c, v) in self.possibleChanges(state) )

		return children


	def getNextState(self, states):

		best = max(states.values())

		return [state for (state,utility) in states.items() if utility == best][0]

		

	def updateState(self, state):
		return self.assign(state)
		

	def assign(self, state):

		nextState = state.copy()

		while self.assignable(nextState):
			for (row, col) in self.assignable(nextState):
				if self.possibleValues(nextState, row, col):
					nextState.set(row, col, self.possibleValues(nextState, row, col)[0])
		
					
		return nextState
			

def solve(puzzle):
	state = State(puzzle)
	solver = Sudoku()
	solution = solver.solve(state)
	return solution

b = State.BLANK

# easy
easy = [
	[b, 6, 7,  8, b, b,  3, b, b],
	[b, 3, b,  b, b, b,  9, 7, b],
	[4, b, 5,  6, 3, 7,  8, 1, b],

	[6, 2, 4,  9, 7, 3,  b, 5, 8],
	[b, 1, b,  b, 6, 8,  7, b, 9],
	[b, 7, b,  5, 1, 2,  4, b, b],

	[7, b, 2,  b, 4, b,  b, b, b],
	[3, b, 1,  b, b, 9,  b, 8, b],
	[9, b, b,  1, b, 5,  2, b, 7]
]

medium = [
	[b, 4, b,  b, b, b,  b, b, 6],
	[9, 6, b,  5, 1, b,  b, b, 7],
	[b, b, b,  b, 8, 9,  b, b, b],

	[3, 7, b,  9, 2, b,  b, 5, b],
	[b, b, b,  b, b, b,  b, b, b],
	[b, 2, b,  b, 7, 6,  b, 9, 3],

	[b, b, b,  4, 9, b,  b, b, b],
	[8, b, b,  b, 6, 1,  b, 7, 9],
	[7, b, b,  b, b, b,  b, 6, b]
]

hard = [
	[b, b, 3,  b, b, 6,  b, 5, b],
	[b, b, b,  5, b, 4,  b, b, 8],
	[b, b, b,  3, b, b,  9, b, 1],

	[b, b, 4,  b, b, 3,  b, 7, b],
	[5, b, b,  b, b, b,  b, b, 2],
	[b, 8, b,  7, b, b,  5, b, b],

	[3, b, 2,  b, b, 7,  b, b, b],
	[7, b, b,  4, b, 8,  b, b, b],
	[b, 9, b,  2, b, b,  3, b, b]
]

# evil
evil = [
	[b, 5, b,  1, b, b,  3, b, b],
	[7, b, b,  b, b, b,  b, b, 5],
	[b, b, b,  b, 3, 9,  b, 1, b],

	[b, b, 4,  b, b, b,  b, 8, 3],
	[b, 7, b,  b, 4, b,  b, 2, b],
	[6, 8, b,  b, b, b,  4, b, b],

	[b, 4, b,  9, 2, b,  b, b, b],
	[5, b, b,  b, b, b,  b, b, 9],
	[b, b, 9,  b, b, 8,  b, 3, b]
]


start = time.time()
print(solve(easy))
duration = (time.time() - start) * 1000
print("took {} millis".format(round(duration)))



